package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class TaskWithCities {
    public static void main(String[] args) {

        List<String> inputCities = Arrays.asList("London", "Nlondo", "Tokyo", "Kyoto", "Moscow", "Donlon", "Scowmo", "Liverpool");

        Map<String, List<String>> map = groupWithStreams(inputCities);
//        Map<String, List<String>> map = groupWithoutStreams(inputCities);
        List<List<String>> lists = new ArrayList<>(map.values());

        lists.sort((s1, s2) -> s2.size() - s1.size()); // Optional: sorting for more accurate result
        System.out.println(lists);
    }

    private static Map<String, List<String>> groupWithStreams(List<String> inputCities) {
        return inputCities.stream()
                .collect(Collectors.groupingBy(o -> sortString(o.toLowerCase()),
                        Collectors.toList())
                );
    }

    public static String sortString(String inputString) {
        char[] tempArray = inputString.toCharArray();
        Arrays.sort(tempArray);
        return new String(tempArray);
    }

    private static void groupWithoutStreams(List<String> inputCities) {
        Map<String, List<String>> result = new HashMap<>();

        for (String city : inputCities) {
            String sortedCity = sortString(city.toLowerCase());

            result.computeIfAbsent(sortedCity, k -> new ArrayList<>());
            result.get(sortedCity).add(city);
        }
        System.out.println(result);
    }
}