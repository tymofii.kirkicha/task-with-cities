package com.company

fun main() {
    val input: List<String> = listOf("London", "Nlondo", "Tokyo", "Kyoto", "Moscow", "Donlon", "Scowmo", "Liverpool")

    val result = input.groupBy { it.sortString() }.values

    println(result)
}

fun String.sortString() : String {
    return this.toLowerCase().toCharArray().sorted().toString()
}
